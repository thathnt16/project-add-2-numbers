/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package mybignumber;

import java.util.Scanner;

/**
 *
 * @author DELL
 */
public class MyBigNumber {

    /**
     * @return
     */
    public static int compareTwoInteger(int first_number, int second_number) {
        if (first_number >= second_number) {
            return first_number;
        }
        return second_number;
    }

    public static String sum(String stn1, String stn2) {
        String result = "";
        int length_stn1 = stn1.length();
        int length_stn2 = stn2.length();
        int max_length = compareTwoInteger(length_stn1, length_stn2);
        int temp = 0;

        for (int i = 0; i < max_length; i++) {
            int first_num = 0;
            int second_num = 0;
            
            if (length_stn1 > 0) {
                char character_stn1 = stn1.charAt(length_stn1 - 1);
                first_num = Integer.parseInt(String.valueOf(character_stn1));
            }
            if (length_stn2 > 0) {
                char character_stn2 = stn2.charAt(length_stn2 - 1);
                second_num = Integer.parseInt(String.valueOf(character_stn2));
            }
            int number = 0;

            int total = first_num + second_num + temp;
            if (total >= 10) {
                number = total % 10;
                result = result + String.valueOf(number);
                temp = 1;
            } else {
                number = total;
                result = result + String.valueOf(number);
                temp = 0;
            }
            if (stn1.length() > 0) {
                stn1 = stn1.substring(0, stn1.length() - 1);
                length_stn1 = length_stn1 - 1;
            }
            if (stn2.length() > 0) {
                stn2 = stn2.substring(0, stn2.length() - 1);
                length_stn2 = length_stn2 - 1;
            }
        }
        String str = new StringBuffer(result).reverse().toString();
        return str;
    }

    public static void main(String[] args) {
        // TODO code application logic here
        String first_number;
        String second_number;
        Scanner scanner = new Scanner(System.in);
        first_number = scanner.nextLine();
        second_number = scanner.nextLine();
        System.out.print(first_number + "+" + second_number + "=");
        String result = sum(first_number,second_number);
        System.out.println(result);
    }

}
